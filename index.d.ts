declare module '*.png';
declare module '*.css';
declare module '*.svg';

type LOGINTYPE = string;
type GETREGISTEREDUSERS = string;
type CREATENEWUSER = string;
type GETOPENEXCHANGECURRENCIES = string;
type GETOPENEXCHANGERATES = string;
type AUTHENTICATE = string;
type LOGOUT = string;
type GETALLTRANSACTIONS = string;
type CREATENEWTRANSACTION = string;
type GETSINGLETRANSACTION = string;
type DELETETRANSACTION = string;
type UPDATETRANSACTION = string;
type GETLOGGEDUSER = string;
type CONVERTCURRENCY = string;

interface GetLoggedUser{
  type: GETLOGGEDUSER;
  payload: I_UserFrontEnd;
}

interface LogOutUser{
  type: LOGOUT;
  payload: null;
}

interface AuthenticateUser{
  type: AUTHENTICATE;
  payload: Boolean;
}

interface CreateNewUser {
  type: CREATENEWUSER;
  payload?: any;
}

interface CreateNewTransaction {
  type: CREATENEWTRANSACTION;
  payload?: any;
}

interface GetRegisteredUsers {
  type: GETREGISTEREDUSERS;
  payload: Number;
}

interface GetOpenExchangeCurrencies {
  type: GETOPENEXCHANGECURRENCIES;
  payload: any;
}

interface GetOpenExchangeRates {
  type: GETOPENEXCHANGERATES;
  payload: any;
}

interface GetAllTransactions {
  type: GETALLTRANSACTIONS;
  payload: any;
}

interface GetSingleTransaction {
  type: GETSINGLETRANSACTION;
  payload: {};
}

interface DeleteTransaction {
  type: DELETETRANSACTION;
  payload: any
}

interface UpdateTransaction {
  type: UPDATETRANSACTION;
  payload: any
}

interface ConvertCurrency {
  type: CONVERTCURRENCY;
  payload: any;
}

type ProjectActionTypes = GetRegisteredUsers | 
                          CreateNewUser | 
                          GetOpenExchangeCurrencies | 
                          AuthenticateUser | 
                          LogOutUser | 
                          GetOpenExchangeRates |
                          CreateNewTransaction |
                          GetSingleTransaction |
                          DeleteTransaction |
                          UpdateTransaction |
                          GetLoggedUser |
                          GetAllTransactions;

declare module 'FormTypes' {

  import { BaseFieldProps, WrappedFieldProps, Field } from 'redux-form';

  interface ValuesInterface {
    name: string;
    lastName: string;
    userName: string;
    gender: string;
    mail: string;
    password: string;
    confirm: string;
    baseCurrency: string;
  }

  interface ErrorFormInterface {
    name?: string;
    lastName?: string;
    userName?: string;
    gender?: string;
    mail?: string;
    password?: string;
    confirm?: string;
    baseCurrency?: string;
  }

  interface FieldInterface extends Partial< WrappedFieldProps & BaseFieldProps & { type: string }> {
  }
  // field: WrappedFieldProps & BaseFieldProps & { type: string } directo en Componente Field
}

interface TransactionInterface extends Document {
  title: string;
  description: string;
  amount: number;
  date: Date;
  user: Partial<UserInterface>;
  _id?: string;
}

interface UserInterface {
  name: String;
  lastName: String;
  userName: String;
  gender: String;
  profilePic: String;
  mail: String;
  password?: string;
  passSalt?: string;
  createdAt: Number;
  baseCurrency: string;
  isAuthenticated?: boolean;
  transactions?: Array<Object>;
  _id?: string;
  __v?: number;
}

interface RatesInterface {
  disclaimer: string;
  license: string;
  timestamp: number;
  base: string;
  rates: {};
}

interface AppInterface {
  registeredUsers?: number;
  isAuthenticated?: boolean;
  currentBaseCurrency?: string;
}

interface StoreState extends _.Dictionary<{}> {
  app: AppInterface;
  user: UserInterface;
  currencies: {};
  form: any;
  latestRates: RatesInterface;
}

interface Test2 {
  a: number;
  b: number;
}

interface Transaction_ValidationTypes {
  title: string;
  description: string;
  categorie: string;
  amount: string;
}

interface I_UserFrontEnd extends Partial<UserInterface>{  }

interface I_TransactionFrontEnd extends Partial<TransactionInterface> { }

// THIS IS THE OLDWAY
// interface I_UserFrontEnd {
//   name: string,
//   lastName: string,
//   userName: string,
//   mail: string,
//   password: string,
//   confPass: string,
// }
