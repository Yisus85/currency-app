import * as React from 'react';
import * as ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { BrowserRouter as Router } from 'react-router-dom';
import { createStore, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';
import { composeWithDevTools } from 'remote-redux-devtools';
import rootReducer from './App/reducers/index';
import App from './App/components/AppComponent/AppComponent';

const composeEnhancers = composeWithDevTools({ name: 'currencyCachus'});
const store = createStore(rootReducer, /* preloadedState, */ composeEnhancers(
  applyMiddleware(thunk),
  // other store enhancers if any
));

ReactDOM.render(
  <div>
    <Provider store={store}>
      <Router>
        <App />
      </Router>
    </Provider>
  </div>
  ,
  document.getElementById('root') as HTMLElement
);

// ReactDOM.render(
//   <div>
//       <Provider store={store}>
//         <Router>
//         <div>
//           <div className="container">
//             <Switch>
//               <ProtectedRoute path="/transaction/new" component={NewTransactionComponent} />
//               <ProtectedRoute path="/transaction/:id" component={SingleTransactionComponent}/>
//               <ProtectedRoute path="/profile/user" component={UserComponent}/>
//               <ProtectedRoute path="/profile/" component={ProfileComponent}/>
//               <Route path="/register" component={RegisterComponent} />
//               <Route path="/logout" component={HomeComponent} />
//               <Route path="/login" component={LoginComponent} />
//               <Route path="/" component={HomeComponent} />
//               {/* LAS DOS FORMAS DE RENDERIZAR UN SUBCOMPONENT CON ROUTE (RENDER-function Y COMPONENT-class)
//               <ProtectedRoute path="/profile" component={ProfileComponent} render={(routerProps) => <ProfileComponent />} /> */
//               }
//             </Switch>
//           </div>
//         </div>
//         </Router>
//       </Provider>
//   </div>
//   ,
//   document.getElementById('root') as HTMLElement
// );

export default store;
