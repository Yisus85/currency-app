import { CONSTANTS } from '../constants';

export default function (isAuthenticated: boolean = false, action: ProjectActionTypes) {

  switch (action.type) {
    
    case CONSTANTS.AUTHENTICATEUSER:
      // const newState = { ...state, ...action.payload, isAuthenticated: true };
      return action.payload;
    
    case CONSTANTS.LOGOUT:
      return action.payload;
    
    default:
      return false;
  }
}
