import { CONSTANTS } from '../constants';

export default function (rates: Object = {}, action: ProjectActionTypes) {

  switch (action.type) {
    case CONSTANTS.GETOPENEXCHANGERATES:
      return  { ...action.payload };

    default:
      return rates;

  }
}
