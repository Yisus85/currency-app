import { CONSTANTS } from '../constants';

export default function (app: AppInterface = { registeredUsers: 0, isAuthenticated: false }, action: ProjectActionTypes) {

  switch (action.type) {
    case CONSTANTS.GETREGISTEREDUSERS:
      return { ...app, registeredUsers: action.payload };

    case CONSTANTS.AUTHENTICATEUSER:
      // const newState = { ...state, ...action.payload, isAuthenticated: true };
      return { ...app, isAuthenticated: action.payload };

    case CONSTANTS.LOGOUT:
      return { ...app, isAuthenticated: action.payload };

    case CONSTANTS.CONVERTCURRENCY:
      return { ...app, currentBaseCurrency: action.payload };

    default:
      return app;

  }
}
