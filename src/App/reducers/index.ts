import { combineReducers } from 'redux';
import { reducer as formReducer } from 'redux-form';
import appReducer from './reducer-app';
import currencyReducer from './reducer-currency';
import userReducer from './reducer-user';
import ratesReducer from './reducer-rates';

const rootReducer = combineReducers({
  currencies: currencyReducer,
  app: appReducer,
  user: userReducer,
  form: formReducer,
  latestRates: ratesReducer,
});

export default rootReducer;
