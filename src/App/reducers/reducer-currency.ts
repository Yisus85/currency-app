import { CONSTANTS } from '../constants';

export default function (currencies: Object = {}, action: ProjectActionTypes) {

  switch (action.type) {
    case CONSTANTS.GETOPENEXCHANGECURRENCIES:
      return { ...action.payload };

    default:
      return currencies;

  }
}
