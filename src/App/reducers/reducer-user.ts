import { CONSTANTS } from '../constants';

export default function (user: I_UserFrontEnd = {}, action: ProjectActionTypes) {

  switch (action.type) {

    case CONSTANTS.GETLOGGEDUSER:
      return action.payload;

    case CONSTANTS.GETALLTRANSACTIONS:
      const transactions = { ...user, transactions: action.payload };
      return transactions;
    
    case CONSTANTS.CONVERTCURRENCY:
      return { ...user, baseCurrency : action.payload };
    
    case CONSTANTS.LOGOUT:
      return {};

    default:
      return user;
  }
}
