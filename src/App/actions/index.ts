import axios, { AxiosPromise } from 'axios';
import { Dispatch } from 'react-redux';
import { ThunkAction } from 'redux-thunk';
import { CONSTANTS, OE_CURRENCIES_API, OE_LATESTRATE_API } from '../constants';
import * as _ from 'lodash';

export function acLogOut(callback: Function): ThunkAction<void, LogOutUser, null> {
  const request: AxiosPromise = axios.post('/api/1.0/logout');

  return (dispatch: Dispatch<LogOutUser>): void => {
    request.then(
      (data) => {
        dispatch({ type: CONSTANTS.LOGOUT, payload: false });
        alert('You have succesfully logged out :3... Bye!');
        callback();
      }
    ).catch(e => alert(e));
  };
}

export function acGetLoggedUser(cb: Function): ThunkAction<void, GetLoggedUser, null> {
  const request: AxiosPromise = axios.get('/api/1.0/profile');

  return (dispatch: Dispatch<GetLoggedUser>): void => {
    request.then(
      ({ data }) => {
        if (data.status !== 404) {
          // tslint:disable
          const { user } = data;
          if (user) {
            dispatch({ type: CONSTANTS.GETLOGGEDUSER, payload: user });
            cb();
          }
        } else {
          alert(`Error: ${data.status} ${data.error.message}`);
        }
      }
    )
      .catch(e => alert(e));
  };
}

function setAuthenticationValue(val: boolean): AuthenticateUser {
  return { type: CONSTANTS.AUTHENTICATEUSER, payload: val };
}
export function acLoginAuthenticate(userInfo: I_UserFrontEnd, callback: Function): ThunkAction<void, AuthenticateUser, null> {
  const request: AxiosPromise = axios.post('/api/1.0/login', userInfo);
  return (dispatch: Dispatch<AuthenticateUser>): void => {
    request.then(
      ({ data }) => {
        if (data === 'Accepted') {
          dispatch(setAuthenticationValue(true));
          callback();
        } else {
          dispatch(setAuthenticationValue(false));
          alert(`Error: ${data.status} ${data.error.message}`);
        }
      }
    )
      .catch(e => alert('ññoo'));
  };
}

export function acAuthenticateRequest(): ThunkAction<void, AuthenticateUser, null> {
  const request: AxiosPromise = axios.get('/api/1.0/authenticate');
  return (dispatch: Dispatch<AuthenticateUser>): void => {
    request.then(
      ({ data }) => {
        if (data.status && data.status !== 401) {
          dispatch(setAuthenticationValue(true));
        }
      }
    ).catch(e => console.log('Not Logged'));
  };
}

export function acGetRegisteredUsers(): ThunkAction<void, GetRegisteredUsers, null> {
  const request: AxiosPromise = axios.get('/api/1.0/');
  return (dispatch: Dispatch<GetRegisteredUsers>): void => {
    request.then(
      ({ data: { users } }) => {
        dispatch({ type: CONSTANTS.GETREGISTEREDUSERS, payload: users.length });
      }
    );
  };
}

export function acCreateNewUser(userData: I_UserFrontEnd, callback: Function): ThunkAction<void, CreateNewUser, null> {
  const request: AxiosPromise = axios.post('/api/1.0/userRegister', userData);
  return (dispatch: Dispatch<CreateNewUser>): void => {
    request.then(
      ({ data }) => {
        if (data.error) {
          alert(data.error.message);
        } else {
          dispatch({ type: CONSTANTS.CREATENEWUSER });
          alert('Succesfully added a user :3');
        }
      }
    );
  };
}

export function setCurrencyValue(data: Object): GetOpenExchangeCurrencies {
  return { type: CONSTANTS.GETOPENEXCHANGECURRENCIES, payload: data };
}

export function acGetOpenExchangeCurrencies(): ThunkAction<void, GetOpenExchangeCurrencies, null> |
  GetOpenExchangeCurrencies {
  const localStorageData = localStorage.getItem('currencies');
  if (!localStorageData) {
    const request: AxiosPromise = axios.get(OE_CURRENCIES_API);
    return (dispatch: Dispatch<GetOpenExchangeCurrencies>): void => {
      request.then(({ data }) => {
        localStorage.setItem('currencies', JSON.stringify(data)); /*  = JSON.stringify(data); */
        dispatch(setCurrencyValue(data));
      }
      );
    };
  } else {
    // forma con DISPATCH :3
    // return (dispatch: Dispatch<GetOpenExchangeCurrencies>): void => {
    //   dispatch(setCurrencyValue(JSON.parse(localStorageData)));
    // };
    return setCurrencyValue(JSON.parse(localStorageData));
  }
}

export function setRatesValues(data: Object): GetOpenExchangeRates {
  return { type: CONSTANTS.GETOPENEXCHANGERATES, payload: data };
}

export function acGetOpenExchangeRates(): ThunkAction<void, GetOpenExchangeRates, null> | GetOpenExchangeRates {
  const localStorageData = localStorage.getItem('rates');
  if (!localStorageData) {
    const request: AxiosPromise = axios.get(OE_LATESTRATE_API);
    return (dispatch: Dispatch<GetOpenExchangeRates>): void => {
      request.then(({ data }) => {
        localStorage.setItem('rates', JSON.stringify(data));
        dispatch(setRatesValues(data));
      }
      );
    };
  } else {
    return setRatesValues(JSON.parse(localStorageData));
  }
}

export function setAllTransactionsValues(data: Object): GetAllTransactions {
  return { type: CONSTANTS.GETALLTRANSACTIONS, payload: data };
}

export function acGetAllTransactions(aR: number): ThunkAction<void, GetAllTransactions, null> {
  const request: AxiosPromise = axios.get('/api/1.0/transactions');
  return (dispatch: Dispatch<GetAllTransactions>): void => {
    request.then(
      ({ data: { transactions } }) => {
        const _convertedTransactions = _.mapKeys(transactions, '_id');
        dispatch(setAllTransactionsValues(_convertedTransactions));
      }
    );
  };
}

export function setSingleTransactionValue(data: Object): CreateNewTransaction {
  return { type: CONSTANTS.CREATENEWTRANSACTION, payload: data };
}

export function acCreateTransaction(transactionData: I_TransactionFrontEnd, cb: Function): ThunkAction<void, CreateNewTransaction, null> {
  const request: AxiosPromise = axios.post('/api/1.0/newTransaction', transactionData);
  return (dispatch: Dispatch<CreateNewTransaction>): void => {
    request.then(
      ({ data: { transaction } }) => {
        cb();
        dispatch(setSingleTransactionValue(transaction));
      }
    );
  };
}

export function getSingleTransactionValue(data: Object): GetSingleTransaction {
  return { type: CONSTANTS.GETSINGLETRANSACTION, payload: data };
}
export function acFetchSingleTransaction(id: string): ThunkAction<void, GetSingleTransaction, null> {
  const request: AxiosPromise = axios.get(`/api/1.0/${id}`);
  return (dispatch: Dispatch<GetSingleTransaction>): void => {
    request.then(
      ({ data: { transaction } }) => {
        dispatch(getSingleTransactionValue(transaction));
      }
    );
  };
}

export function acConvertCurrency(fromB: string, toB: string, trans: {}): ConvertCurrency {
  return { type: CONSTANTS.CONVERTCURRENCY, payload: toB };
}

export function deleteTransactionValue(data: Object): DeleteTransaction {
  return { type: CONSTANTS.DELETETRANSACTION, payload: data };
}
export function acDeleteTransaction(id: string, cb: Function): ThunkAction<void, DeleteTransaction, null> {
  const request: AxiosPromise = axios.delete(`/api/1.0/${id}`);
  return (dispatch: Dispatch<DeleteTransaction>): void => {
    request.then(
      ({ data: { transaction } }) => {
        cb();
        dispatch(getSingleTransactionValue(transaction));
      }
    );
  };
}

export function updateTransactionValue(data: Object): UpdateTransaction {
  return { type: CONSTANTS.UPDATETRANSACTION, payload: data };
}
export function acUpdateTransaction(id: string): ThunkAction<void, UpdateTransaction, null> {
  const request: AxiosPromise = axios.put(`/api/1.0/${id}`);
  return (dispatch: Dispatch<UpdateTransaction>): void => {
    request.then(
      ({ data: { transaction } }) => {
        dispatch(getSingleTransactionValue(transaction));
      }
    );
  };
}

export function acTestGraphQl(): any {
  console.log('request a graphql desde action creator...');
  const request: AxiosPromise =
    axios({
      url: '/api/1.0/graphql',
      method: 'POST',
      data: {
        query: `
        query {
          transaction(id: "5c3d1f1e5e910f54e7942acd"){
            title
            amount
          }
        }
        `
      },
    });
  return (dispatch: any) => {
    request.then(a => {
      console.log(a.data.data);
      dispatch(getSingleTransactionValue(a.data));
    }).catch(e => {
      console.log('tpm: ERROR DE AUTENTICACION :(');
    });
  }
}