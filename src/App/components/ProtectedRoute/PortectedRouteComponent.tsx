import { Component } from 'react';
import * as React from 'react';
import { Route, Redirect, RouteProps } from 'react-router';
import { connect } from 'react-redux';

class ProtectedRoute extends Component<RouteProps & {isAuthenticated: boolean}, StoreState> {

  render() {
    const isAuthenticated = this.props.isAuthenticated;
    const myComponent = this.props.component;
    const path = this.props.path;
    return (
      isAuthenticated ?
        (
          <Route 
            path={path} 
            component={myComponent}
          />
        ) :
        <Redirect to="/" />
    );
  }
}

const mapStateToProps = (state: StoreState) => {
  return {isAuthenticated: state.app.isAuthenticated};
};

// tslint:disable-next-line:no-any
export default connect
  <
    // mapStateToProps
    { isAuthenticated: boolean | undefined},
    // mapDispatchToProps
    {},
    RouteProps,
    StoreState
  >
(mapStateToProps, {})(ProtectedRoute);
