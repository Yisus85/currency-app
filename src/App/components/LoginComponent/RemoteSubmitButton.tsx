import * as React from 'react';
import { connect } from 'react-redux';
import { submit } from 'redux-form';

const style = {
  display: 'block',
  margin: '20px auto',
};

// tslint:disable
const RemoteSubmitButton = ({ dispatch }: any) => (
  <button type="submit"
    className="btn btn-primary"
    style={style}
    onClick={() => dispatch(submit('remoteSubmit'))}
    value="Submit"
  >
    Submit
  </button>
);
//                                  ^^^^^^^^^^^^ name of the form}
export default connect()(RemoteSubmitButton);