import * as React from 'react';
import { Component } from 'react';
import { Field, BaseFieldProps, WrappedFieldProps, reduxForm, ConfigProps } from 'redux-form';
import { acLoginAuthenticate } from '../../actions/index';
import validate from './validate';
import { InjectedFormProps } from 'redux-form';
import { RouterProps } from 'react-router';
import { ThunkAction } from 'redux-thunk';
import { connect } from 'react-redux';
import LoginStyles from './LoginComponent.css';
import { Link, RouteComponentProps } from 'react-router-dom';

type PropsLoginComponent = RouterProps &
  InjectedFormProps &
  RouteComponentProps<{}> &
  { acLoginAuthenticate(u: I_UserFrontEnd, cb: Function): ThunkAction<void, AuthenticateUser, null> };

class LoginUser extends Component<PropsLoginComponent> {

  onSubmit = (userInfo: I_UserFrontEnd) => {
    this.props.acLoginAuthenticate(userInfo, () => {
      this.props.history.push('/profile/');
    });
  }

  onCancel = () => {
    this.props.reset();
  }

  renderField(field: BaseFieldProps & WrappedFieldProps): JSX.Element {
    const { label, meta: { touched, error } } = field;
    let thetype: string = 'text';
    if (label === 'password') {
      thetype = 'password';
    }

    const condclassName = `${LoginStyles.input} ${touched && error ? 'alert-danger' : ''}`;
    return (
      <div className="form-group" role="alert">
        <label className={LoginStyles.label}>{field.label}</label>
        <input
          className={condclassName}
          type={thetype}
          placeholder={`Insert your ${label}`}
          {...field.input}
        />
        <div className="alert-warning">
          {touched ? error : ''}
        </div>
      </div>
    );
  }

  render(): JSX.Element {
    const { handleSubmit } = this.props;
    return (
      <form onSubmit={handleSubmit(this.onSubmit)}>
        <Field label="e-mail" name="mail" component={this.renderField} />
        <Field label="password" name="password" component={this.renderField} />
        <div>
          <Link to="/" onClick={this.onCancel} className={LoginStyles.HomeMainButtons}>Cancel</Link>
          <button type="submit" className={LoginStyles.HomeMainButtons}>Submit</button>
        </div>
      </form>
    );
  }

}

export default reduxForm({ validate, form: 'PostsNewForm' } as ConfigProps)(
  connect(null, { acLoginAuthenticate })(LoginUser)
);
