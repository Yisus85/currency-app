import * as FormTypes from 'FormTypes';

const validate = (values: FormTypes.ValuesInterface): FormTypes.ErrorFormInterface => {
  const errors: FormTypes.ErrorFormInterface = {};
  if (!values.password) {
    errors.password = 'Required';
  }
  if (!values.mail) {
    errors.mail = 'Required';
  } else if (!/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(values.mail)) {
    errors.mail = 'Invalid email address';
  }
  return errors;
};

export default validate;
