import * as React from 'react';
import '../../../index.css';
import HomeComponent from '../HomeComponent/HomeComponent';
import LoginComponent from '../LoginComponent/LoginComponent';
import RegisterComponent from '../RegisterComponent/RegisterComponent';
import NewTransactionComponent from '../ProfileComponent/Transactions/NewTransactionComponent/NewTransactionComponent';
import SingleTransactionComponent from '../ProfileComponent/Transactions/SingleTransactionComponent/SingleTransactionComponent';
import ProtectedRoute from '../ProtectedRoute/PortectedRouteComponent';
import ProfileComponent from '../ProfileComponent/ProfileComponent';
import UserComponent from '../ProfileComponent/UserComponent/UserComponent';
import { withRouter, RouteProps, RouteComponentProps, Switch, Route } from 'react-router-dom';
import { SwitchProps, Redirect } from 'react-router';
import { connect } from 'react-redux';
import { acAuthenticateRequest, acGetOpenExchangeCurrencies, acGetRegisteredUsers, acGetOpenExchangeRates } from '../../actions/index';
import { ThunkAction } from 'redux-thunk';
import ReactLoading from 'react-loading';
import { Dispatch } from 'redux';

class App extends React.Component<
                                  {isAuthenticated: boolean} &
                                  SwitchProps &
                                  RouteProps &
                                  {
                                    acGetOpenExchangeCurrencies: () => Dispatch<GetOpenExchangeCurrencies>,
                                    acGetRegisteredUsers: () => Dispatch<GetRegisteredUsers>,
                                    acGetOpenExchangeRates: () => Dispatch<GetOpenExchangeRates>,
                                    acAuthenticateRequest: () => ThunkAction<void, AuthenticateUser, null>
                                  } &
                                  RouteComponentProps<{}>
                                  ,
                                  StoreState
> {

  componentDidMount() {
    this.props.acAuthenticateRequest();
    this.props.acGetRegisteredUsers();
    this.props.acGetOpenExchangeCurrencies();
    this.props.acGetOpenExchangeRates();
  }

  render() {
    const isAuthenticated = this.props.isAuthenticated;
    
    return (
      <div className="container">
        <Switch>
          <ProtectedRoute path="/transaction/new" component={NewTransactionComponent} />
          <ProtectedRoute path="/transaction/:id" component={SingleTransactionComponent} />
          <ProtectedRoute path="/profile/user" component={UserComponent} />
          <ProtectedRoute path="/profile/" component={ProfileComponent} />
          <Route 
            path="/register" 
            render={(props) => { 
                return isAuthenticated ?
                <div>
                  <div className="Loading">
                      <ReactLoading type="spokes" color="#FF406D" width="100" height="20" />
                  </div>
                  
                  <Redirect
                    to="/profile"
                  /> 
                </div>
                :
                <RegisterComponent 
                  {...props} 
                />;
              }
            }
          />
          <Route
            path="/login"
            // tslint:disable-next-line:no-any
            render={(props: any) => {
              return isAuthenticated ?
                <div>
                  <div className="Loading">
                    <ReactLoading type="spokes" color="#FF406D" width="100" height="20" />
                  </div>
                  <Redirect
                    to="/profile"
                  />
                </div> :
                <LoginComponent
                  {...props}
                />;
            }
            }
          />
          <Route
            path="/"
            render={(props) => {
              // tslint:disable
              return isAuthenticated ?
                <div>
                  <div className="Loading">
                    <ReactLoading type="spokes" color="#FF406D" width="100" height="20" />
                  </div>
                  <Redirect
                    to="/profile"
                  />
                </div> :
                <HomeComponent
                  {...props}
                />;
            }
            }
          />
          <Route path="/logout" component={HomeComponent} />
        </Switch>
      </div>
    );
  }
}

const mapStateToProps = (state: StoreState) => {
  return { isAuthenticated: state.app.isAuthenticated };
};

export default withRouter(connect(  mapStateToProps, 
                                    { acAuthenticateRequest, 
                                      acGetOpenExchangeCurrencies, 
                                      acGetRegisteredUsers, 
                                      acGetOpenExchangeRates 
                                    })(App));
