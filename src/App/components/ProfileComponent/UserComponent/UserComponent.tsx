import * as React from 'react';
// import * as ReactDOM from 'react-dom';
import { connect } from 'react-redux';
import { acLogOut, acGetOpenExchangeRates } from '../../../actions/index';
import { withRouter, RouteComponentProps } from 'react-router';
import UserComponentStyles from './UserComponent.css';
import UserProfilePic from '../../../../Assets/user.svg';
import AllTransactionsComponent from '../Transactions/AllTransactionsComponent/AllTransactionsComponent';
import { ThunkAction } from 'redux-thunk';

class UserComponent extends React.Component<
  RouteComponentProps<{}> &
  { user: I_UserFrontEnd } &
  {
    acLogOut(c: Function): ThunkAction<void, LogOutUser, null>
  } & 
  {
    acGetOpenExchangeRates(): GetOpenExchangeRates | ThunkAction<void, GetOpenExchangeRates, null>
  }
  > {

  onLogOut = () => {
    if (this.props.acLogOut) {
      this.props.acLogOut(() => {
        this.props.history.replace('/');
      });
    }
  }
  
  getNewRates = () => {
    const rates = localStorage.getItem('rates');
    if (rates) {
      localStorage.removeItem('rates');
    }
    this.props.acGetOpenExchangeRates();
    alert('The currency rates have been updated');
  }

  render() {
    return (
      <div>
        <div className={UserComponentStyles.UserSideBar}>
          <img src={UserProfilePic} alt="" />
          <div>
            <button onClick={this.onLogOut}>logout</button>
          </div>
          <div className={UserComponentStyles.UserInfo}>
            <p>
              {this.props.user.name}
            </p>
            <p>
              {this.props.user.lastName}
            </p>
            <p>{this.props.user.mail}</p>
            <p>{this.props.user.password}</p>
            <p 
              style={{ color: '#FF406D', backgroundColor: 'white', fontSize: '2em'}}
            >{this.props.user.baseCurrency}
            </p>
            <p>{this.props.user.gender}</p>
            <a className="btn btn-secondary" onClick={this.getNewRates}>Update Rates</a>
          </div>
        </div>
        <AllTransactionsComponent/>
      </div>

    );
  }
}

function mapStateToProps(state: StoreState) {
  return {
    user: state.user,
  };
}

// tslint:disable-next-line:no-any
// export default connect(mapStateToProps, { acLogOut })(ProfileComponent as any);
export default withRouter(
  // tslint:disable-next-line:max-line-length
  connect<
    Partial<StoreState>,
    {
      acLogOut(c: Function): ThunkAction<void, LogOutUser, null>
    } &
    {
      acGetOpenExchangeRates(): GetOpenExchangeRates | ThunkAction<void, GetOpenExchangeRates, null>
    },
    RouteComponentProps<{}>,
    StoreState
  >(mapStateToProps, { acLogOut, acGetOpenExchangeRates })(UserComponent)
);
