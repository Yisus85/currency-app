import * as React from 'react';
import { Component } from 'react';
import { RouteComponentProps } from 'react-router';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import { acFetchSingleTransaction, acDeleteTransaction, acUpdateTransaction } from '../../../../actions/index';
import { ThunkAction } from 'redux-thunk';
import ReactLoading from 'react-loading';

export type SingleTransactionComponentType = StoreState &
  RouteComponentProps<{ id: number }> &
  { user_id: string} &
  { _transaction: I_TransactionFrontEnd } &
  { acFetchSingleTransaction: (id: number) => ThunkAction<void, GetSingleTransaction, null> } &
  { acUpdateTransaction: (id: number) => ThunkAction<void, UpdateTransaction, null> } &
  { acDeleteTransaction: (id: number, cb: Function) => ThunkAction<void, DeleteTransaction, null> };

class SingleTransaction extends Component<SingleTransactionComponentType> {

  componentDidMount() {
    const { id } = this.props.match.params;
    this.props.acFetchSingleTransaction(id);
  }

  onDelete = () => {
    const { id } = this.props.match.params;
    this.props.acDeleteTransaction(id, () => {
      this.props.history.push(`/profile/${this.props.user_id}`);
    });
  }

  render() {
    if (this.props._transaction
) {
      return (
        <div>
          <Link className="btn btn-secondary" to={`/profile/${this.props.user_id}`}>
            Back
          </Link>

          <button
            className="btn btn-danger float-sm-right"
            onClick={this.onDelete}
          >
            Delete Post
          </button>
          <div className="singleTransactionContainer">
            <h3>{this.props._transaction.title}</h3>
            <h6>{this.props._transaction.description}</h6>
            <p>{this.props._transaction.amount}</p>
          </div>

        </div>
      );
    }
    return (
      <div className="Loading">
        <ReactLoading type="spokes" color="#FF406D" width="100" height="20" />
      </div>
    );
  }
}

// tslint:disable-next-line:no-any
function mapStateToProps({ user: { transactions }, user: { _id } }: StoreState, ownProps: RouteComponentProps<{id: string}>) {
  if (transactions) {
    return {
      _transaction: transactions[ownProps.match.params.id],
      user_id: _id
    };
  } else {
    return {
      _transaction: {},
      user_id: '0'
    };
  }
}

// tslint:disable-next-line:no-any
export default connect(mapStateToProps, { acFetchSingleTransaction, acDeleteTransaction, acUpdateTransaction })(SingleTransaction as any);
