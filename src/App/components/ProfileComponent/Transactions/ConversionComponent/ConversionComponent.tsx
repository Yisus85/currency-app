import * as React from 'react';
import { Component } from 'react';
import ConversionStyle from './ConversionComponent.css';

interface MappedProps {
  currencyRates: Object;
  currencyNames: Object;
}

interface OwnProps {
  conversion: number;
  fromCurrency: string;
  toCurrency: string;
  valueToConvert: number;
}

class ConversionComponent extends Component<MappedProps, OwnProps> {

  // tslint:disable-next-line:no-any
  constructor(props: any) {
    super(props);
    this.state = {
      conversion: 1,
      fromCurrency: 'USD',
      toCurrency: 'USD',
      valueToConvert: 1
    };
  }

  renderOptions = () => {
    if (this.props.currencyNames) {
      let currencyNames = this.props.currencyNames;
      const values: Array<Object> = Object.entries(currencyNames);
      return values.map(([key, value]: Array<string>, idx: number) => {
        // let selection = key === 'USD' ? true : false;
        return (
          <option
            key={idx}
            value={key}
          // selected={selection}
          >
            {`${value} (${key})`}
          </option>
        );
      });
    }
    return <div />;
  }

  // tslint:disable-next-line
  setValue = ({target:{value}}: React.ChangeEvent<HTMLInputElement>) => {
    // e.persist();

    // this.setState({ valueToConvert: +value ? +value : 0 }, () => {
    this.setState({ valueToConvert: +value || 0 }, () => {
      const { fromCurrency, toCurrency, valueToConvert } = this.state;
      this.convert(fromCurrency, toCurrency, valueToConvert); 
    });
  }

  convert(from: string, to: string, value: number) {
    const { currencyRates } = this.props;
    const result = (value / currencyRates[from]) * currencyRates[to];
    this.setState({ conversion: result });
  }

  // tslint:disable-next-line
  set_fromCurrency = (event: any) => {
    this.setState({ fromCurrency: event.target.value }, () => {
      const { fromCurrency, toCurrency, valueToConvert } = this.state;
      this.convert(fromCurrency, toCurrency, valueToConvert);
    });
  }

  // tslint:disable-next-line
  set_toCurrency = (event: any) => {
    this.setState({ toCurrency: event.target.value}, () => {
      const { fromCurrency, toCurrency, valueToConvert } = this.state;
      this.convert(fromCurrency, toCurrency, valueToConvert);
    });
  }

  render() {

    return (
      <div className={ConversionStyle.MainClass}>
        <h3>Currency convertion</h3>
        <hr />
        <h4>From:</h4>
        <input type="number" defaultValue="1" onChange={this.setValue} />

        <select
          name="userCurrencies"
          id="baseCurrency"
          onChange={this.set_fromCurrency}
          defaultValue="USD"
        >
          {this.renderOptions()}
        </select>
        <hr />
        <h4>To:</h4>
        <select
          name="userCurrencies"
          id="converCurrency"
          onChange={this.set_toCurrency}
          defaultValue="USD"
        >
          {this.renderOptions()}
        </select>
        <hr />
        <h4>Result:</h4>
        <h3>{this.state.conversion}</h3>
      </div>
    );
  }
}

export default ConversionComponent;
