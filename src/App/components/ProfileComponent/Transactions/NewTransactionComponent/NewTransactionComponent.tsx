import * as React from 'react';
import { Component } from 'react';
import { connect } from 'react-redux';
import { Field, reduxForm, ConfigProps, BaseFieldProps, WrappedFieldProps, InjectedFormProps } from 'redux-form';
import { Link } from 'react-router-dom';
import { acCreateTransaction } from '../../../../actions/index';
import { RouterProps } from 'react-router';
import { ThunkAction } from 'redux-thunk';
import NewTransactionStyle from './NewTransactionComponent.css';

// Necesitamos declarar un tipo que contenga la interfaz exacta de los nuevos
// tipos de las propiedades a utilizar en nuestro componente u.u...
type PropsNewComponent = RouterProps &
  InjectedFormProps &
  { user_id: string } &
  { acCreateTransaction: (t: I_TransactionFrontEnd, cb: Function) => ThunkAction<void, CreateNewTransaction, null> };

// Component siempre recibe primero (las Propiedades como primer argumento, y el estado como segundo argumento)
class NewTransaction extends Component<PropsNewComponent> {

  renderField(field: BaseFieldProps & WrappedFieldProps & {min?: string, placeholder?: string, step?: string}): JSX.Element {
    const { step, placeholder, min, label, meta: { touched, error }, input: {value} } = field;
    const className = `form-control-lg ${touched && error ? 'alert-danger' : ''}`;

    let type = 'text';

    if (label === 'Amount') {
      type = 'number';
    } else {
      type = 'text';
    }

    return (
      <div className={`form-group`} role="alert">
        <label>{field.label}</label>
        <input
          value={parseFloat(value).toFixed(2)}
          step={step}
          placeholder={placeholder}
          min={min}
          className={className}
          type={type}
          {...field.input}
        />
        <div className="alert-warning">
          {touched ? error : ''}
        </div>
      </div>
    );
  }

  onSubmit = (transactionData: I_TransactionFrontEnd) => {
    const {user_id} = this.props;
    this.props.acCreateTransaction(transactionData, () => {
      this.props.history.push(`/profile/${user_id}`);
    });
  }

  render(): JSX.Element {

    const { handleSubmit, user_id } = this.props;

    return (
      <form className={`${NewTransactionStyle.NewTransactionContainer}`} onSubmit={handleSubmit(this.onSubmit)} >
        <Field placeholder="Enter a Title" label="Title for Transaction" name="title" component={this.renderField} />
        <Field placeholder="Enter a description" label="Description" name="description" component={this.renderField} />
        <Field placeholder="Outcome" label="Categorie" name="categorie" component={this.renderField} />
        <Field placeholder="0.00" min="0.00" step="0.01" label="Amount" name="amount" component={this.renderField} />

        <button type="submit" className="btn btn-primary">Submit</button>
        <Link to={`/profile/${user_id}`} className="btn btn-danger">Cancel</Link>
      </form>
    );
  }
}

function mapStateToProps(state: Partial<StoreState>) {
  if (state.user) {
    return { user_id: state.user._id };
  } else {
    return { user_id: '0' };
  }
}

function validate(values: Transaction_ValidationTypes): Transaction_ValidationTypes {
  const errors: Transaction_ValidationTypes = { title: '', categorie: '', description: '', amount: '' };

  if (!values.title) {
    errors.title = 'Enter a title!';
  }

  if (!values.description) {
    errors.description = 'Enter a description!';
  }

  if (!values.categorie) {
    errors.categorie = 'Enter a categorie!';
  }

  if ((!values.amount) || (!values.amount.match(/[0-9]+/))) {
    errors.amount = 'Enter a correct amount!';
  }

  if (parseInt(values.amount, 10) === 0) {
    values.amount = (((parseInt(values.amount, 10)) * 1000) / 100).toString();
    errors.amount = 'Provide a valid amount!';
  }

  return errors;
}

export default reduxForm({ validate, form: 'TransactionsNewForm' } as ConfigProps)(
  /**
   * first argument:  mapStateToProps()
   * aquí estamos diciendo que no nos suscribiremos a los updates del store de redux,
   * dado que el primer parámetro es mapStateToProps, y es una función que pasa el
   * state de redux a las props del componente
   * 
   * second argument: mapDispatchToProps(): función u objeto
   * al pasar un objeto cada función dentro del mismo será tratado como un redux action
   * creator.
   */

  // tslint:disable-next-line:max-line-length
  // connect<{}, { acCreateTransaction: (b: I_TransactionFrontEnd, cb: Function) => ThunkAction<void, CreateNewTransaction, null> }, RouterProps & InjectedFormProps, StoreState>

  connect(mapStateToProps, { acCreateTransaction })(NewTransaction)
);
