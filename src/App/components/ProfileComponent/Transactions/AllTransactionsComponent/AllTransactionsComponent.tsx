import { Component } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import { acGetAllTransactions, acConvertCurrency, acTestGraphQl } from '../../../../actions';
import * as React from 'react';
import * as _ from 'lodash';
import AllTransactionsStyle from './AllTransactionsComponent.css';
import { ThunkAction } from 'redux-thunk';
import ConversionComponent from '../ConversionComponent/ConversionComponent';
// import axios from 'axios';

class AllTransactions extends Component<{ acGetAllTransactions: (aR: number) => ThunkAction<void, GetAllTransactions, null> } &
{ acConvertCurrency: (f: string, t: string, tr: {}) => ConvertCurrency } &
// tslint:disable-next-line:no-any
{ acTestGraphQl: () => {} } &
{ loggedUser: I_UserFrontEnd } &
{ currencyNames: {} } &
{ rates: {} } &
  StoreState> {
  componentDidMount(): void {
    const theBaseRate = this.props.loggedUser.baseCurrency ? this.props.loggedUser.baseCurrency : '';
    this.props.acGetAllTransactions(this.props.rates[theBaseRate.toString()]);
  }

  renderOptions = () => {
    let currencyNames = this.props.currencyNames;
    const values: Array<Object> = Object.entries(currencyNames);
    return values.map(([key, value]: Array<string>, idx: number) => {
      return (
        <option
          key={idx}
          value={key}
        >
          {`${key} (${value})`}
        </option>
      );
    });
  }

  // tslint:disable-next-line:no-any
  getSelectValue = (event: any) => {
    const { loggedUser: { baseCurrency, transactions }, rates } = this.props;
    if (baseCurrency && transactions) {
      const fromBase: string = baseCurrency.toString();
      const toBase = event.target.value;
      this.props.acConvertCurrency(fromBase, toBase, {});
      // tslint:disable-next-line:forin
      for (const key in transactions) {
        const element: I_TransactionFrontEnd = transactions[key];
        if (element.amount) {
          element.amount = (element.amount / rates[fromBase]) * rates[toBase];
        }
      }
    }
  }

  renderTransactions(): Array<Object> {
    // let sumExpense = 0;
    return _.map((this.props.loggedUser.transactions), (transaction: Partial<TransactionInterface>): JSX.Element => {
      if (transaction.amount) {

        let amount = '0';

        if (transaction.amount >= 1) {
          amount = transaction.amount.toFixed(2);
        } else {

          const exp = Math.ceil(Math.log10(transaction.amount) * -1);
          amount = transaction.amount.toFixed(exp + 3);
        }

        return (
          <tr className="" key={transaction._id}>
            <th>
              <Link to={`/transaction/${transaction._id}`}>
                {transaction.title}
              </Link>
            </th>
            <td>
              {transaction.description}
            </td>
            <td className={AllTransactionsStyle.theAmount}>
              {amount}
            </td>
          </tr>
        );
      }
      return <div />;
    }
    );
  }

  renderTotal() {
    let sum = 0;
    if (this.props.loggedUser.transactions) {
      _.map((this.props.loggedUser.transactions), (trans: I_TransactionFrontEnd) => {
        if (trans.amount) {
          sum += trans.amount;
        }
      });
    }
    if (sum >= 1) {
      return sum.toFixed(2);
    } else if (sum === 0) {
      return sum.toFixed(2);
    } else {
      const exp = Math.ceil(Math.log10(sum) * -1);
      return sum.toFixed(exp + 3);
    }
  }
  // tslint:disable
  testTransaction = () => {
    console.log('testTransaction from component...');
    this.props.acTestGraphQl();
  }

  render(): JSX.Element {
    const rates = { 'currencyNames': this.props.currencyNames, 'currencyRates': this.props.rates };
    return (
      <div className={AllTransactionsStyle.MainClass}>
        <h3>Transactions</h3>
        <hr />
        <select
          name="userCurrencies"
          id="currencies"
          // ref={(select) => { this.select = select; }}
          onChange={this.getSelectValue}
        >
          {this.renderOptions()}
        </select>
        <hr />

        <table className="table table-striped table-dark">
          <caption>last transactions</caption>
          <thead>
            <tr>
              <th scope="col">Name</th>
              <th scope="col">Description</th>
              <th scope="col">Amount</th>
            </tr>
          </thead>
          <tbody>
            {this.renderTransactions()}
          </tbody>
          <tfoot>
            <tr>
              <th scope="row" >Total</th>
              <td />
              <td >{this.renderTotal()}</td>
            </tr>
          </tfoot>
        </table>
        <hr />
        <div className="text-md-center">
          <Link className="btn btn-primary btn-lgls" to="/transaction/new">
            Add a Transaction
          </Link>
          <button className="btn btn-primary btn-lgls" onClick={this.testTransaction}>
            call graphQL
          </button>
        </div>
        <ConversionComponent {...rates} />
      </div>
    );
  }
}

function mapStateToProps(state: StoreState) {
  return {
    appCurrency: state.app.currentBaseCurrency,
    loggedUser: state.user,
    currencyNames: state.currencies,
    rates: state.latestRates.rates
  };
}

export default connect<Partial<StoreState>,
  {
    acGetAllTransactions(aR: number): ThunkAction<void, GetAllTransactions, null>,
    acConvertCurrency(a: string, b: string, t: {}): ConvertCurrency,
    acTestGraphQl(): {}
  },
  {}, StoreState>(mapStateToProps, { acGetAllTransactions, acConvertCurrency, acTestGraphQl })(AllTransactions);
