import * as React from 'react';
import { connect } from 'react-redux';
import { acGetLoggedUser } from '../../actions/index';
import { withRouter, RouteComponentProps } from 'react-router';
import { ThunkAction } from 'redux-thunk';
import ReactLoading from 'react-loading';

class ProfileComponent extends React.Component<
  RouteComponentProps<{}> &
  { user: I_UserFrontEnd } &
  {
    acGetLoggedUser(cb: Function): ThunkAction<void, GetLoggedUser, null>
  }> {

  componentDidMount() {
    this.props.acGetLoggedUser(() => {
      this.props.history.push(`/profile/user`);
    });
  }

  render() {
    return (
      <div className="Loading">
        <ReactLoading type="spokes" color="#FF406D" width="100" height="20" />
      </div>
    );
  }
}

function mapStateToProps(state: Partial<StoreState>) {
  return { user: state.user };
}

// tslint:disable-next-line:no-any
// export default connect(mapStateToProps, { acLogOut })(ProfileComponent as any);
export default withRouter(
  // tslint:disable-next-line:max-line-length
  connect

    <Partial<StoreState>,
    { acGetLoggedUser(c: Function): ThunkAction<void, GetLoggedUser, null> },
    {},
    StoreState>

    (mapStateToProps, { acGetLoggedUser })(ProfileComponent)
);
