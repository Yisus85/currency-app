import * as React from 'react';
import { Field, reduxForm, ConfigProps } from 'redux-form';
import validate from './validate';
import renderField from './renderField';
import RegisterStyles from './RegisterComponent.css';

// tslint:disable-next-line:no-any
const renderError = ({ meta: { touched, error } }: any ) =>
  touched && error ? <span className={RegisterStyles.Errors}>{error}</span> : false;

// tslint:disable-next-line:no-any
const WizardFormSecondPage: Function = ({ handleSubmit, previousPage }: HTMLFormElement): JSX.Element => {
  return (
    <form onSubmit={handleSubmit}>
      <Field name="mail" type="email" component={renderField} label="Email" />
      <div>
        <label>Sex</label>
        <div>
          <label>
            <Field name="gender" component="input" type="radio" value="male" />
            {' '}
            Male
          </label>
          <label>
            <Field name="gender" component="input" type="radio" value="female" />
            {' '}
            Female
          </label>
          <Field name="gender" component={renderError} />
        </div>
      </div>
      <div>
        <button type="button" className={RegisterStyles.HomeMainButtons} onClick={previousPage}>
          Previous
        </button>
        <button type="submit" className={RegisterStyles.HomeMainButtons}>Next</button>
      </div>
    </form>
  );
};

export default reduxForm({
  form: 'wizard', //                 <------ same form name
  destroyOnUnmount: false, //        <------ preserve form data
  forceUnregisterOnUnmount: true, // <------ unregister fields on unmount
  validate,
// tslint:disable-next-line:no-any
}as ConfigProps)(WizardFormSecondPage as any);
