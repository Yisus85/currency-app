import * as React from 'react';
import { Component } from 'react';
import WizardFormFirstPage from './WizardFormFirstPage';
import WizardFormSecondPage from './WizardFormSecondPage';
import WizardFormThirdPage from './WizardFormThirdPage';
import { acCreateNewUser } from '../../actions';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';

// tslint:disable-next-line:no-any
class WizardForm extends Component<any, any> {
  // tslint:disable-next-line:no-any
  constructor(props: any) {
    super(props);
    this.state = {
      page: 1,
    };
  }
  nextPage = () => {
    this.setState({ page: this.state.page + 1 });
  }

  previousPage = () => {
    this.setState({ page: this.state.page - 1 });
  }

  sendInformation2Server = (userInfo: I_UserFrontEnd) => {
    this.props.acCreateNewUser(userInfo);
    this.props.history.push('/login');
  }

  render() {
    // const { onSubmit } = this.props;
    const { page } = this.state;
    const previousPage = {'previousPage': this.previousPage};

    return (
      <div>
        {page === 1 && <WizardFormFirstPage onSubmit={this.nextPage} />}
        {page === 2 &&
          <WizardFormSecondPage
            {... previousPage}
            onSubmit={this.nextPage}
          />}
        {page === 3 &&
          <WizardFormThirdPage
            {...previousPage}
            onSubmit={this.sendInformation2Server}
          />}
      </div>
    );
  }
}

// tslint:disable-next-line:no-any
export default withRouter(connect<any, any, any, any>(null, {acCreateNewUser})(WizardForm as any));
