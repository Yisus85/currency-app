import * as React from 'react';
import { Field, reduxForm, ConfigProps } from 'redux-form';
import validate from './validate';
import renderField from './renderField';
import RegisterStyles from './RegisterComponent.css';
import { Link } from 'react-router-dom';

const WizardFormFirstPage = (props: HTMLFormElement): JSX.Element => {
  const { handleSubmit } = props;
  const onCancel = () => {
    props.reset();
  };

  return (
    <form onSubmit={handleSubmit}>
      <Field 
        name="name"
        type="text"
        component={renderField}
        label="First Name"
      />
      <Field
        name="lastName"
        type="text"
        component={renderField}
        label="Last Name"
      />

      <div>
        <Link to="/" onClick={onCancel} className={RegisterStyles.HomeMainButtons}>Cancel</Link>
        <button type="submit" className={RegisterStyles.HomeMainButtons}>Next</button>
      </div>
    </form>
  );
};

export default reduxForm({
  form: 'wizard', //                 <------ same form name
  destroyOnUnmount: false, //        <------ preserve form data
  forceUnregisterOnUnmount: true, // <------ unregister fields on unmount
  validate,
// tslint:disable-next-line:no-any
} as ConfigProps)(WizardFormFirstPage as any);
