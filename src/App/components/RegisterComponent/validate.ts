import * as FormTypes from 'FormTypes';

const validate = (values: FormTypes.ValuesInterface): FormTypes.ErrorFormInterface => {
  const errors: FormTypes.ErrorFormInterface = {};
  if (!values.name) {
    errors.name = 'Required';
  }
  if (!values.password) {
    errors.password = 'Required';
  }
  if (!values.confirm) {
    errors.confirm = 'Required';
  }
  if (!values.lastName) {
    errors.lastName = 'Required';
  }
  if (!values.mail) {
    errors.mail = 'Required';
  } else if (!/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(values.mail)) {
    errors.mail = 'Invalid email address';
  }
  if (!values.gender) {
    errors.gender = 'Required';
  }
  if (!values.baseCurrency) {
    errors.baseCurrency = 'Required';
  }
  if (values.password !== values.confirm) {
    errors.confirm = 'Passwords must match';
  }

  return errors;
};

export default validate;
