import * as React from 'react';
import { Field, reduxForm } from 'redux-form';
// import { connect } from 'react-redux';
import validate from './validate';
import renderField from './renderField';
import RegisterStyle from './RegisterComponent.css';
// const colors = ['Red', 'Orange', 'Yellow', 'Green', 'Blue', 'Indigo', 'Violet'];

const getCurrenciesData = () => {
  const currencies = localStorage.getItem('currencies');
  if (currencies) {
    const data = JSON.parse(currencies);
    const values: Array<Object> = Object.entries(data);
    return values.map(([key, value]: Array<string>, idx: number) => {
      return (
        <option
          key={idx}
          value={key}
        >
          {`${key} (${value})`}
        </option>
      );
    });
  }
  return <div />;
};

// tslint:disable-next-line:no-any
const renderColorSelector = ({ input, meta: { touched, error } }: any) => (
  <div>
    <select {...input}>
      <option value="">currency...</option>
      {getCurrenciesData()}
    </select>
    {touched && error && <span style={RegisterStyle.Errors}>{error}</span>}
  </div>
);

// tslint:disable-next-line:no-any
const WizardFormThirdPage: any = (props: any): any => {
  // tslint:disable-next-line:no-any
  const { handleSubmit, pristine, previousPage, submitting }: any = props;
  return (
    <form onSubmit={handleSubmit}>
      <div>
        <label>Select your BASE currency</label>
        <Field name="baseCurrency" component={renderColorSelector} />
      </div>
      <Field name="password" type="password" component={renderField} label="Password" />
      <Field name="confirm" type="password" component={renderField} label="Confirm Password" />
      <div>
        <button type="button" className={`previous ${RegisterStyle.HomeMainButtons}`} onClick={previousPage}>
          Previous
        </button>
        <button className={RegisterStyle.HomeMainButtons} type="submit" disabled={pristine || submitting}>Submit</button>
      </div>
    </form>
  );
};
export default reduxForm({
  form: 'wizard', //                 <------ same form name
  destroyOnUnmount: false, //        <------ preserve form data
  forceUnregisterOnUnmount: true, // <------ unregister fields on unmount
  validate,
// tslint:disable-next-line:no-any
}as any)(WizardFormThirdPage as any);
