import * as React from 'react';
// import { WrappedFieldProps, BaseFieldProps } from 'redux-form';
import * as FormTypes from 'FormTypes';
import RegisterStyles from './RegisterComponent.css';

// tslint:disable-next-line
const renderField = (field: FormTypes.FieldInterface): JSX.Element => {
  if (field.meta) {
    const { input, label, type, meta: { touched, error } } = field;
    let inputStyle = RegisterStyles.label;
    if (type === 'text' || 'email') {
      inputStyle = RegisterStyles.TextInput;
    }
    return (
      <div>
        <label className={RegisterStyles.label}>{label}</label>
        <div>
          <input className={inputStyle} {...input} placeholder={label} type={type} />
          {touched && error && <div className="alert alert-danger">{error}</div>}
        </div>
      </div>
    );
  }
  return (
    <div>
    lala
  </div>);
};

export default renderField;
