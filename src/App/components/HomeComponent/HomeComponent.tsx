import * as React from 'react';
import { Component } from 'react';
import logoLogin from '../../../Assets/login-logo.png';
import HomeComponentStyle from './HomeComponent.css';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { RedirectProps } from 'react-router';

class HomeComponent extends Component<RedirectProps &
  { style: React.CSSProperties | string } &
  StoreState> {

  render(): JSX.Element {
    return (
      <div>
        <main>
          <img src={logoLogin} className={HomeComponentStyle.imgHome} alt="" />
          <h1 className={HomeComponentStyle['display-4']}>{this.props.app.registeredUsers}</h1>
          <h1 className={HomeComponentStyle.HappyUsers}>Happy Users</h1>
          <div className={HomeComponentStyle.MainQuote}>
            <p className="lead"><cite>"Beware of little expenses; a small leak will sink a great ship."</cite></p>
            <hr className="my-4" />
            <p>Benjamin Franklin</p>
          </div>
          <p className="lead">
            <Link
              to={'/login'}
              className={HomeComponentStyle.HomeMainButtons}
              href="#"
              role="button"
            >
              Sign in
            </Link>
            <Link
              to={'/register'}
              className={HomeComponentStyle.HomeMainButtons}
              href="#"
              role="button"
            >
              Register
            </Link>
          </p>
        </main>
      </div>
    );
  }
}

function mapStateToProps(state: StoreState): Partial<StoreState> {
  return {
    app: state.app,
    isAuthenticated: state.app.isAuthenticated
  };
}

// Puedo 
// connect<MapStateToProps, MapDispatchToProps, OwnProps, store>
// tslint:disable-next-line:max-line-length
export default connect(mapStateToProps, { })(HomeComponent);
