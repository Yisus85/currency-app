FROM node:8

#Creat app directory
WORKDIR /usr/src/app

COPY package*.json ./
RUN npm install

COPY ./build .

EXPOSE 8000

CMD ["node","./server/server.js"]
