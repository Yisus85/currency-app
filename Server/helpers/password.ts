// <reference path="../ServerTypings/index.d.ts"/>
// import { HashPasswordFunction, HashPasswordCallbackFunction } from 'ServerApp';

/*************** d.ts file *************/

interface HashPasswordFunction {
  (password: string | undefined, salt: string | undefined, callback: HashPasswordCallbackFunction): HashPasswordCallbackFunction|void;
}

interface HashPasswordCallbackFunction {
  (error: Error | null, hash: string, salt?: string): Function;
}

/***************************************/

import * as crypto from 'crypto';

const LEN: number = 256,
  SALT_LEN: number = 64,
  ITERATIONS: number = 10000,
  DIGEST: string = 'sha256';

export const hashPassword: HashPasswordFunction = function( password: string, 
                                                            salt: string, 
                                                            callback: HashPasswordCallbackFunction): HashPasswordCallbackFunction|void {

  let len = LEN / 2;

  if (salt !== '') {
    return crypto.pbkdf2( password, 
                          salt, 
                          ITERATIONS, 
                          len, DIGEST, 
                          (err, derivedKey) => {
                            if (err) {
                              return callback(err, ''); // if it fails, we notify the user with err
                            }
                            return callback(null, derivedKey.toString('hex'));
                          }
                        );
  } else { // Si es una password nueva
    return crypto.randomBytes ( SALT_LEN / 2, // le proporcionamos la longitud de la Salt
                                (err, newSalt) => {
                                  if (err) {
                                    return callback(err, ''); // if it fails, we notify the user with err
                                  } 
                                  salt = newSalt.toString('hex');
                                  return crypto.pbkdf2( password, 
                                                        salt, 
                                                        ITERATIONS, 
                                                        len, 
                                                        DIGEST,
                                                        (err2: Error, derivedKey: Buffer) => {
                                                          if (err2) {
                                                            return callback(err2, ''); // if it fails, we notify the user with err2
                                                          }
                                                          
                                                          return callback(null, derivedKey.toString('hex'), salt);
                                                          }
                                                      );
                                }
                              );
  }
};
