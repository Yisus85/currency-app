import { UserModel } from 'ServerModelsInterface';
import { User } from '../models/userModel';
import * as express from 'express';
import { MongoError } from 'mongodb';
import { transaction } from '../routes/routes';

//tslint:disable
export class UserControl {
  model: UserModel = User;

  getAll = async (req: express.Request, res: express.Response) => {
    try {
      const docs = await this.model.find({}, 'name mail');
      // tslint:disable-next-line:no-console
      res.send({ status: 200, users: docs });
    } catch (err) {
      res.send({ error: { name: err.code, message: 'Could not get the Data' } });
    }
  }

  // insert into database
  insert = async (req: express.Request, res: express.Response) => {
    const objUser = new this.model(req.body);
    try {
      const data = await objUser.save();
      res.send({ status: 201, user: data });
    } catch ({ name }) {
      const message = 'e-mail account already registered';
      res.send({ error: { name, message } });
    }
  }

  test = (req: express.Request, res: express.Response) => {
    res.json({ a: 5, b: 'this is a test' });
  }

  getLoggedUser = async (req: express.Request, res: express.Response) => {
    const { user: { id: _id } } = req;
    try {
      const user = await this.model.findById({ _id });
      res.send({ status: 202, user });
    } catch (err) {
      res.send({ status: 404, error: { name: err, message: 'No registered user' } });
    }
  }

  authenticate = (req: express.Request, res: express.Response) => {
    if (req.user) {
      res.sendStatus(200);
    } else {
      res.sendStatus(401);
    }
  }

  login = async (req: express.Request, res: express.Response) => {
    const { mail: mailR, password: passwordR } = req.body;
    try {
      const user = await this.model.findOne({ mail: mailR }, '+password +passSalt');

      if (this.model.comparePassword && user) {
        return this.model.comparePassword(passwordR, user, (err1: MongoError, data: { token: string }) => {
          if (data && !err1) {
            res.cookie('token', data.token);
            return res.sendStatus(202);
          } else {
            return res.send({ status: 404, error: err1 });
          }
        }
        );
      } else {
        res.send({ status: 404, error: new MongoError('No registered User') });
      }
    } catch (err) {
      res.send({ status: 404, error: { name: err, message: 'No registered user' } });
    }
  }

  logout = async (req: express.Request, res: express.Response) => {
    transaction.setInternalCookies({ token: '' });
    res.clearCookie('token');
    res.sendStatus(200);
  }

}
