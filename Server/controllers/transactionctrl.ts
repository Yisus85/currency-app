import { Transaction } from '../models/transactionModel';
import * as express from 'express';
import { TransactionModel } from 'ServerModelsInterface';

export class TransactionControl {
  model: TransactionModel = Transaction;
  internalCookies: { token: string };

  test = (req: express.Request, res: express.Response) => {
    res.json({ a: 5, b: 'this is a test' });
  }

  // getAll from database
  getAll = async (req: express.Request, res: express.Response) => {
    const { user: { id: _id } } = req;
    try {
      const docs = await this.model.find({ user: _id })/* .explain('executionStats') */;
      res.send({ status: 200, transactions: docs });
    } catch (err) {
      res.send({ error: { name: err.code, message: 'Could not get the Data' } });
    }
  }

  // insert into database
  insert = async (req: express.Request, res: express.Response) => {
    const obj = new this.model(req.body);
    const { user: { id: _id } } = req;
    obj.user = _id;

    try {
      const transaction = await obj.save();
      res.send({ status: 201, transaction });
    } catch ({ name }) {
      const message = 'could not save data!...';
      res.send({ error: { name, message } });
    }
  }

  getSingle = async (req: express.Request, res: express.Response) => {
    const { params: { transactionId } } = req;
    try {
      const transaction = await this.model.find({ _id: transactionId }, 'title description amount date')/* .explain('executionStats') */;
      res.send({ status: 200, transaction });
    } catch (err) {
      res.send({ error: { name: err.code, message: 'Could get any the Data' } });
    }
  }

  updateTransaction = async (req: express.Request, res: express.Response) => {
    const { user: { id: _id } } = req;
    const { params: { transactionId } } = req;

    try {
      const transaction = await this.model.findOneAndUpdate({ _id: transactionId, user: _id }, req.body)/* .explain('executionStats') */;
      res.send({ status: 200, transaction });
    } catch (err) {
      res.send({ error: { name: err.code, message: 'Could not update the Transaction' } });
    }
  }

  deleteTransaction = async (req: express.Request, res: express.Response) => {
    const { user: { id: _id } } = req;
    const { params: { transactionId } } = req;
    try {
      const transaction = await this.model.findOneAndRemove({ _id: transactionId, user: _id })/* .explain('executionStats') */;
      res.send({ status: 200, transaction });
    } catch (err) {
      res.send({ error: { name: err.code, message: 'Could not delete the Transaction' } });
    }
  }

  setInternalCookies(value: { token: string; }) {
    this.internalCookies = value;
  }
  getInternalCookies() {
    return this.internalCookies;
  }
}
