import { MONGODBCONFIG, MONGODBURL } from '../constants';
import * as mongoose from 'mongoose';

const { MONGODBURL: DBURI, DBUSER, DBPASSWORD, DBREPLICASET, DBAUTHSOURCE } = process.env;
const { user, pass, ssl, replicaSet, authSource } = MONGODBCONFIG;

const URI = DBURI || MONGODBURL;
const CONFIG = {
  user: DBUSER || user,
  pass: DBPASSWORD || pass,
  ssl: true || ssl,
  replicaSet: DBREPLICASET || replicaSet,
  authSource: DBAUTHSOURCE || authSource
} || MONGODBCONFIG;

mongoose.connect(URI, CONFIG).then(
  (a) => {
    // tslint:disable
    console.log('Database connection established n.n!...');
  },
  (e) => {
    // tslint:disable
    console.log('valar Morghulis!!!');
    console.log(e);
  }
);

export const db = mongoose.connection;
// tslint:disable-next-line:no-any
(<any>mongoose).Promise = global.Promise;
