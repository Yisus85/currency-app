
// export const MONGODBURL = 'mongodb://localhost:27017/currencyApp';

// tslint:disable-next-line:max-line-length
export const MONGODBURL = 'mongodb://clustercurrency-shard-00-00-he1pv.mongodb.net:27017,clustercurrency-shard-00-01-he1pv.mongodb.net:27017,clustercurrency-shard-00-02-he1pv.mongodb.net:27017/currencyApp';
export type MONGODBURL = typeof MONGODBURL;

// mongoose ^5 no necesita useMongoClient:true
// export const MONGODBCONFIG = { useMongoClient: true, user: 'cachus', pass: 'ahlulu' };
export const MONGODBCONFIG = { user: 'cachus', pass: 'ahlulu', ssl: true, replicaSet: 'clusterCurrency-shard-0', authSource: 'admin' };
// export type MONGODBCONFIG = typeof MONGODBCONFIG;

export const WEBTOKENSECRET = 'amimevalebergaloquetudigas';
export type WEBTOKENSECRET = typeof WEBTOKENSECRET;
