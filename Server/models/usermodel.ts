import { WEBTOKENSECRET } from '../constants';
import * as mongoose from 'mongoose';
import { hashPassword } from '../helpers/password';
import * as jwt from 'jsonwebtoken';
import * as ServerModelsInterface from 'ServerModelsInterface';

const userSchema: ServerModelsInterface.UserSchema = new mongoose.Schema({
  name: String,
  lastName: String,
  userName: String,
  gender: { type: String, default: 'Female' },
  profilePic: { type: String, default: '../../assets/img/profile-default.jpg' },
  mail: { type: String, unique: true, lowercase: true, trim: true },
  password: { type: String, select: false },
  baseCurrency: { type: String, required: true },
  passSalt: { type: String, select: false, default: '' },
  createdAt: {
    type: Number, default: function () {
      let now = new Date().getTime();
      return now;
      // Date.UTC(now.getUTCFullYear(), now.getUTCMonth(), now.getUTCDay(),  now.getUTCHours(), now.getUTCMinutes(), now.getUTCSeconds());
    }
  },
  connected: { type: Boolean, default: false },
});

userSchema.set('toJSON', {
  // tslint:disable-next-line:no-any
  transform: function(doc: ServerModelsInterface.UserModel, user: ServerModelsInterface.UserInterface, options: any) { 
    delete user.passSalt;
    delete user.password;
    delete user._id;
    return user;
  }});

userSchema.pre(
  'save',
  function (this: ServerModelsInterface.UserFrontEndInterface, next: ServerModelsInterface.NextFunction) {
    const user: ServerModelsInterface.UserFrontEndInterface = this;
    // let { password, passSalt, isModified }: UserFrontEndInterface = user;
    if (!user.isModified('password')) { return next(); }
    hashPassword(
      user.password,
      user.passSalt,
      (err: Error, hash: string, salt: string) => {
        if (err) { return next(err); }
        user.password = hash;
        user.passSalt = salt;
        next();
      }
    );
  }
);

userSchema.statics.comparePassword = function (receivedPassword: string, dataBaseUser: ServerModelsInterface.UserInterface, cb: Function) {
  hashPassword(
    receivedPassword, dataBaseUser.passSalt,
    (hashError: Error, hash: string) => { 
      if (hashError) {
        return cb(hashError, null);
      }

      /******** OJOO MUCHO OJO **********/
      if (hash === dataBaseUser.password) {
        // dataBaseUser.password = undefined;
        // // this.password = undefined;
        // dataBaseUser.passSalt = undefined;
        // // this.passSalt = undefined;

        const token = jwt.sign( { id: dataBaseUser._id }, WEBTOKENSECRET, { expiresIn: '480m' });

        return cb(null, { token });

      } else {
        return cb({ success: false, message: 'Wrong Password!' });
      }
    }

  );
};

export const User: ServerModelsInterface.UserModel = mongoose.model('User', userSchema);
