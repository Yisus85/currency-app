import { TransactionSchema, TransactionModel } from 'ServerModelsInterface';
import * as mongoose from 'mongoose';

const transactionSchema: TransactionSchema = new mongoose.Schema({
  title: {
    type: String,
    required: true
  },

  description: {
    type: String,
    required: true
  },

  amount: {
    type: Number,
    required: true
  },

  date: {
    type: Number, default: function () {
      const now = new Date().getTime();
      return now;
      // Date.UTC(now.getUTCFullYear(), 
      // now.getUTCMonth(), now.getUTCDay(),  now.getUTCHours(), now.getUTCMinutes(), now.getUTCSeconds());
    }
  },

  // Add a reference to the user to whom this transaction belongs
  user: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'User',
    required: true
  }
});

export const Transaction: TransactionModel = mongoose.model('Transaction', transactionSchema);
