import * as express from 'express';
import * as path from 'path';
import { db } from './configs/mongoose.config';
import * as bodyParser from 'body-parser';
import * as cookieParser from 'cookie-parser';
let engine = require('react-view-engine');

import defineRoutes from './routes/routes';

const server = express();

engine.initJSX();

db.on('error', console.error.bind(console, 'Connection DataBase error: '));
db.once('open', () => {
  // tslint:disable-next-line
  console.log('Connected to MongoDB');

  const mainFolder = path.join(__dirname, '../');

  // tslint:disable-next-line
  console.log(mainFolder);
  // tslint:disable-next-line
  // console.log(engine);

  server.use(express.static(mainFolder)); // define static folder for server client files
  server.set('views', mainFolder);
  server.set('port', (process.env.PORT || 8000));
  server.set('view engine', 'js');
  server.engine('js', engine.engine);
  server.use(express.static(__dirname + '../public'));
  
  server.use(bodyParser.json());
  server.use(bodyParser.urlencoded({ extended: false }));
  server.use(cookieParser());

  defineRoutes(server); // setting routes
  server.get('/*', (req, res) => {
    // tslint:disable
    // console.log('**********************');
    // console.log(req);
    res.render('index');
  });
  // const app = 
  server.listen(server.get('port'), () => {
    // tslint:disable-next-line
    console.log('Server running on cloud... DE MIERGAS');
    // console.log('Server running on ' + server.get('port'));
  });
});
