import * as express from 'express';
import { UserControl } from '../controllers/userctrl';
import { TransactionControl } from '../controllers/transactionctrl';
import * as expressJwt from 'express-jwt';
import { WEBTOKENSECRET } from '../constants';
import * as graphqlHTTP from 'express-graphql';
import { schema } from '../graphql/Schemas/testSchema';

export const transaction = new TransactionControl();

export default function defineRoutes(_server: express.Application) {
  const router = express.Router();
  const user = new UserControl();

  function getCookieToken(req: express.Request) {
    if (req.cookies && req.cookies.token) {
      transaction.setInternalCookies(req.cookies);
      return req.cookies.token;
    } else if (transaction.getInternalCookies().token !== undefined) {
      return transaction.getInternalCookies().token;
    }
  }

  _server.use(expressJwt({
    secret: WEBTOKENSECRET,
    getToken: getCookieToken
  }).unless({
    path: [
      '/api/1.0/',
      '/api/1.0/login',
      '/api/1.0/register',
      '/api/1.0/userRegister',
      '/api/1.0/test',
    ]
  }));
  // protected Routes
  router.route('/').get(user.getAll);
  router.route('/graphql').get(graphqlHTTP({ schema, /* rootValue: root, */ graphiql: true }));
  router.route('/graphql').post(graphqlHTTP({ schema, /* rootValue: root, */ graphiql: true }));
  router.route('/test').get(user.test);
  router.route('/authenticate').get(user.authenticate);
  router.route('/login').post(user.login);
  router.route('/logout').post(user.logout);
  router.route('/profile').get(user.getLoggedUser);
  router.route('/userRegister').post(user.insert);
  router.route('/transactions').get(transaction.getAll);
  router.route('/newTransaction').post(transaction.insert);
  router.route('/:transactionId').get(transaction.getSingle);
  router.route('/:transactionId').put(transaction.updateTransaction);
  router.route('/:transactionId').delete(transaction.deleteTransaction);

  // defining the use of /api/1 for versioning control
  _server.use('/api/1.0', router);

}
