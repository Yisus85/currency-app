import { GraphQLSchema, GraphQLObjectType } from 'graphql';
import { transactionQuery } from '../Queries/transaction.query';
import { transactionsQuery } from '../Queries/transactions.query';

const QueryType = new GraphQLObjectType({
  name: 'Query',
  description: '...',
  fields: () => ({
    transaction: transactionQuery,
    transactions: transactionsQuery
  })
});

export const schema = new GraphQLSchema(
  {
    query: QueryType
  }
);
