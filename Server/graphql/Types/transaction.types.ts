import { GraphQLObjectType, GraphQLString, GraphQLInt } from 'graphql';
export const TransactionType = new GraphQLObjectType({
  name: 'Transaction',
  description: '',
  fields: () => ({
    title: {
      type: GraphQLString,
      resolve: (transaction) => {
        return transaction.title;
      }
    },
    description: {
      type: GraphQLString,
      resolve: (transaction) => {
        return transaction.description;
      }
    },
    amount: {
      type: GraphQLInt,
      resolve: (transaction) => {
        return transaction.amount;
      }
    }
  })
});
