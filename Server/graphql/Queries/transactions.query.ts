import { GraphQLList } from 'graphql';
import { graphqlTypes } from '../Types/index.types';
import axios from 'axios';
import { BASE_URL } from '../constants';

export const transactionsQuery = {
  type: new GraphQLList(graphqlTypes.TransactionType),
  resolve: () => {
    const request = axios.get(`${BASE_URL}/transactions`);
    return request.then(({ data: { transactions } }) => {
      return transactions;
    }).catch(e => e);
  }
};
