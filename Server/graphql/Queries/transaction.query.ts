import { GraphQLString } from 'graphql';
import { graphqlTypes } from '../Types/index.types';
import axios from 'axios';
import { BASE_URL } from '../constants';

export const transactionQuery = {
  type: graphqlTypes.TransactionType,
  args: { id: { type: GraphQLString } },
  resolve: (a: null, args: { id: string }) => {
    const request = axios.get(`${BASE_URL}${args.id}`);
    return request.then(({ data }) => {
      return data.transaction[0];
    }).catch(e => {
      return e;
    });
  }
};
