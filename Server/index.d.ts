declare module 'node-jsx';

declare module "ServerModelsInterface"{
  import {Document, Model, HookNextFunction, Schema} from 'mongoose';

  interface TransactionInterface extends Document {
    title: string;
    description: string;
    categorie: string;
    amount: number;
    date:Date;
    user: Partial<UserInterface>;
  }

  interface UserInterface extends Document {
    name: String;
    lastName: String;
    userName: String;
    gender: String;
    profilePic: String;
    mail: String;
    password?: string;
    passSalt?: string;
    createdAt: Number;
    connected: Boolean;
  }

  interface UserModel extends Model<UserInterface> {comparePassword?: Function;}
  interface TransactionModel extends Model<TransactionInterface> { }

  interface UserFrontEndInterface extends Partial<UserInterface> { isModified?: any }

  interface NextFunction extends HookNextFunction { test?: number; }

  interface UserSchema extends Schema { }
  interface TransactionSchema extends Schema{ }

}


